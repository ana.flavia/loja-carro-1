using NUnit.Framework;
using Calculadora;
namespace calculadora.unittest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }


        [Test]
        public void Soma_QuandoAcaoForSoma_RetornaSoma()
        {

            //arrange
            var calculadora = new Calculadora.Calculadora();

            calculadora.N1 = 1;
            calculadora.N2 = 2;
            calculadora.Operacao = "Somar";
            //act
            var resultado = calculadora.Soma();
            //assert
            Assert.That(resultado, Is.EqualTo(3));

        }
        [Test]
        public void Subtracao_QuandoAcaoForSubtracao_RetornaSubtracao()
        {

            //arrange
            var calculadora = new Calculadora.Calculadora();

            calculadora.N1 = 1;
            calculadora.N2 = 2;
            calculadora.Operacao = "Subtracao";
            //act
            var resultado = calculadora.Subtracao();
            //assert
            Assert.That(resultado, Is.EqualTo(-1));

        }
        [Test]
        public void Divisao_QuandoAcaoForDivisao_RetornaDivisao()
        {

            //arrange
            var calculadora = new Calculadora.Calculadora();

            calculadora.N1 = 1;
            calculadora.N2 = 2;
            calculadora.Operacao = "Divisao";
            //act
            var resultado = calculadora.Divisao();
            //assert
            Assert.That(resultado, Is.EqualTo(0.5));

        }
        [Test]
        public void Multiplicacao_QuandoAcaoForMultiplicacao_RetornaMultiplicacao()
        {

            //arrange
            var calculadora = new Calculadora.Calculadora();

            calculadora.N1 = 1;
            calculadora.N2 = 2;
            calculadora.Operacao = " Multiplicacao";
            //act
            var resultado = calculadora.Multiplicacao();
            //assert
            Assert.That(resultado, Is.EqualTo(2));

        }
    }
}
    
