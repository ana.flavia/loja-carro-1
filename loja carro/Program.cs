﻿using System;

namespace LojaCarro
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            Carro carro = new Carro();
            carro.Marca = "Renault";
            carro.Modelo = "Sandero";
            carro.Preco = 49000;
            carro.Km = 0;
            carro.Ano = 2017;
            carro.NumeroDonos = 1;


            double valorPacela = carro.ValorParcela(20);
            Console.WriteLine(valorPacela);


            string tipoCarro = carro.TipoCarro();

            Console.WriteLine("\n" + tipoCarro);

            //   Console.WriteLine(carro.Marca);




        }
    }
}