﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LojaCarro
{
    class  Carro
    {
        public int Ano { get; set; }
        public int Numero { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Km { get; set; }
        public double Preco { get; set; }
        public string cor { get; set; }
        public  int NumeroDonos { get; set; }

        public double ValorParcela(int parcelas)
        {
            Console.WriteLine("Calculando Parcela");

            if (parcelas > 24)
                return 0;


            double ValorParcela = this.Preco / parcelas;

            return ValorParcela;
        }

        public string TipoCarro()
        {
            Console.WriteLine("Verificando tipo de carro");
            int IdadeCarro = int.Parse(DateTime.Now.Year.ToString()) - this.Ano;
            double KmPorAno = this.Km / IdadeCarro;


            //verificar se o carro é 0km
            if (this.Km == 0)
            {
                return "zero";
            }
            else if (IdadeCarro <= 3)
            {

                if (this.NumeroDonos == 1)
                {
                    if (KmPorAno <= 20000)
                    {
                        return "seminovo";
                    }
                    else
                    {
                        return "usado";
                    }
                }
                else
                {
                    return "usado";
                }
            }
            else
            {
                return "usado";
            }



            
        }

    }
}
