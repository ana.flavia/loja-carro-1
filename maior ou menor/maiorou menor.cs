﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maior_ou_menor
{
   public class  maioroumenor
    {
        public int Num1 { get; set; }
        public int Num2 { get; set; }
        public int Num3 { get; set; }

        public int Maior()
        {
            // 213
            if (this.Num1 >= this.Num2)
            {
                if (this.Num1 >= this.Num3)
                {
                    return this.Num1;
                }
            }

            if (this.Num2 >= this.Num3)
            {
                return this.Num2;
            }
            else
            {
                return this.Num3;
            }
        }

        public int Menor()
        {
            if (this.Num1 <= this.Num2)
            {
                if (this.Num1 <= this.Num3)
                {
                    return this.Num1;
                }
            }

            if (this.Num2 <= this.Num3)
            {
                return this.Num2;
            }
            else
            {
                return this.Num3;
            }

        }

    }
}