﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculadoraIMC
{
    class Usuario
    {
        public string Nome { get; set; }
        public double Peso { get; set; }
        public double Altura { get; set; }
        public int Idade { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
